/* eslint-disable */
import { useAuth } from '@/store/auth.js'

export default function rotas(to, from, next) {
    if (to.meta?.auth) {
        const auth = useAuth()
        if (!auth.token && !auth.usuario && !auth.ultimoacesso) {
            next({ name: 'login' })
        } else {
            next()
        }
    } else {
        next()
    }
}
