/* eslint-disable */
import { createRouter, createWebHistory } from 'vue-router'
import rotas from './rotas'

function lazyLoad(view) {
  return () => import(`@/views/${view}.vue`);
}

const routes = [
  {
    path: '/home',
    name: 'home',
    title: 'Home',
    component: lazyLoad('HomeView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cliente',
    name: 'cliente',
    title: 'Clientes',
    component: lazyLoad('ClienteView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-cliente',
    name: 'cadastro-cliente',
    title: 'Cadastro de clientes',
    component: lazyLoad('CadastroClienteView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/usuario',
    name: 'usuario',
    title: 'Usuários',
    component: lazyLoad('UsuarioView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-usuario',
    name: 'cadastro-usuario',
    title: 'Cadastro de usuários',
    component: lazyLoad('CadastroUsuarioView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/permissao-usuario',
    name: 'permissao-usuario',
    title: 'Permissões de usuários',
    component: lazyLoad('PermissaoUsuarioView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/perfil',
    name: 'perfil',
    title: 'Perfis',
    component: lazyLoad('PerfilView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-perfil',
    name: 'cadastro-perfil',
    title: 'Cadastro de perfis',
    component: lazyLoad('CadastroPerfilView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/funcao',
    name: 'funcao',
    title: 'Funções',
    component: lazyLoad('FuncaoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-funcao',
    name: 'cadastro-funcao',
    title: 'Cadastro de funções',
    component: lazyLoad('CadastroFuncaoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/especialidade',
    name: 'especialidade',
    title: 'Especialidades',
    component: lazyLoad('EspecialidadeView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-especialidade',
    name: 'cadastro-especialidade',
    title: 'Cadastro de especialidades',
    component: lazyLoad('CadastroEspecialidadeView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/departamento',
    name: 'departamento',
    title: 'Departamentos',
    component: lazyLoad('DepartamentoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-departamento',
    name: 'cadastro-departamento',
    title: 'Cadastro de departamentos',
    component: lazyLoad('CadastroDepartamentoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/funcionario',
    name: 'funcionario',
    title: 'Funcionarios',
    component: lazyLoad('FuncionarioView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-funcionario',
    name: 'cadastro-funcionario',
    title: 'Cadastro de funcionarios',
    component: lazyLoad('CadastroFuncionarioView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/cadastro-lancamento',
    name: 'cadastro-lancamento',
    title: 'Cadastro de lancamentos',
    component: lazyLoad('CadastroLancamentoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/lancamento',
    name: 'lancamento',
    title: 'Lançamentos financeiros',
    component: lazyLoad('LancamentoView'),
    meta: {
      auth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    title: 'Login',
    component: lazyLoad('LoginView'),
    meta: {
      layout: 'empty'
    }
  },
  {
    path: '/logout',
    name: 'logout',
    component: lazyLoad('LoginView'),
    meta: {
      auth: true
    }
  },
  {
    path: "/:pathMatch(.*)",
    component: lazyLoad("PageNotFound"),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach(rotas);

export default router
