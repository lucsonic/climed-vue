import axios from 'axios'
/* eslint-disable */

let api = process.env.VUE_APP_BASE_API

const axiosIntance = axios.create({
    baseURL: api
})

axiosIntance.interceptors.request.use((config) => {
    const token = localStorage.getItem('token')

    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }

    return config
}, (err) => {
    return Promise.reject(err)
})

axiosIntance.interceptors.response.use((response) => {
    return response
}, (error) => {
    if (error.response.status === 401) {
        window.location = '#/home'
    }

    return Promise.reject(error)
})

export default axiosIntance