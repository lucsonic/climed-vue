/* eslint-disable */
import { createApp } from "vue"
import { createPinia } from "pinia"

import App from "./App.vue"
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import '@fortawesome/fontawesome-free/js/all.min.js'
import VueTheMask from "vue-the-mask"
import { Tooltip } from 'bootstrap'

import DefaultLayout from '@/components/layouts/DefaultLayout.vue'
import EmptyLayout from '@/components/layouts/EmptyLayout.vue'

window.$ = window.jquery = require('jquery')

new Tooltip(document.body, {
    selector: "[data-bs-toggle='tooltip']",
    trigger: 'hover'
})

const app = createApp(App)
    .component('default-layout', DefaultLayout)
    .component('empty-layout', EmptyLayout)

app.use(createPinia())
app.use(router)
app.use(VueTheMask)

app.mount("#app")

