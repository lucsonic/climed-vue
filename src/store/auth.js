/* eslint-disable */
import { ref } from 'vue';
import { defineStore } from 'pinia';
import http from '@/services/http.js';

export const useAuth = defineStore('auth', () => {
  const token = ref(localStorage.getItem('token'));
  const usuario = ref(JSON.parse(localStorage.getItem('usuario')));
  const ultimoacesso = ref(JSON.parse(localStorage.getItem('ultimo_acesso')));
  const permissoes = ref(JSON.parse(localStorage.getItem('permissoes')));

  function setToken(accessTokenValue) {
    localStorage.setItem('token', accessTokenValue);
    token.value = accessTokenValue;
  }

  function setUsuario(usuarioValue) {
    localStorage.setItem('usuario', JSON.stringify(usuarioValue));
    usuario.value = usuarioValue;
  }

  function setUltimoAcesso(ultimoAcessoValue) {
    localStorage.setItem('ultimo_acesso', JSON.stringify(ultimoAcessoValue));
    ultimoacesso.value = ultimoAcessoValue;
  }

  function setPermissoes(permissoesValue) {
    localStorage.setItem('permissoes', JSON.stringify(permissoesValue));
    permissoes.value = permissoesValue;
  }

  async function checkToken() {
    try {
      const tokenAuth = 'Bearer ' + token.value;
      const { data } = await http.get("/auth/verify", {
        headers: {
          Authorization: tokenAuth,
        },
      });
      return data;
    } catch (error) {
      console.log(error.response.data);
    }
  }

  function clear() {
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('ultimo_acesso');
    localStorage.removeItem('permissoes');
    token.value = '';
    usuario.value = '';
    ultimoacesso.value = '';
    permissoes.value = '';
  }

  return {
    setToken,
    setUsuario,
    setUltimoAcesso,
    setPermissoes,
    token,
    usuario,
    ultimoacesso,
    checkToken,
    clear
  }
})